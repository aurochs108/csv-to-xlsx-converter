import converter.CsvToXlsxConverter;
import utils.FilesUtil;

import java.io.File;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<File> files = FilesUtil.getFilesInDirectory("src/main/resources/csv");
        files.parallelStream().forEach(file -> {
            String filePath = file.getAbsolutePath().replace("/csv/", "/xlsx/");
            CsvToXlsxConverter.csvToXLSX(file.getAbsolutePath(), filePath.substring(0, filePath.length() - 3) + "xlsx");
        });
        System.out.println("All files converted!");
    }
}
