package converter;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;

public class CsvToXlsxConverter {

    public static void csvToXLSX(String csvLocation, String xlsLocation) {
        try {
            System.out.println("Start converting: " + csvLocation);
            XSSFWorkbook workBook = new XSSFWorkbook();
            XSSFSheet sheet = workBook.createSheet("sheet1");
            String currentLine = null;
            int RowNum = 0;
            BufferedReader br = new BufferedReader(new FileReader(csvLocation));
            while ((currentLine = br.readLine()) != null) {
                String str[] = currentLine.split(",");
                RowNum++;
                XSSFRow currentRow = sheet.createRow(RowNum);
                for (int i = 0; i < str.length; i++) {
                    currentRow.createCell(i).setCellValue(str[i]);
                }
            }

            FileOutputStream fileOutputStream = new FileOutputStream(xlsLocation);
            workBook.write(fileOutputStream);
            fileOutputStream.close();
            System.out.println(csvLocation + " converted!");
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "Exception in try");
        }
    }
}
