package utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.List;

public class FilesUtil {

    public static List<File> getFilesInDirectory(String path) {
        try {
            String[] extensions = new String[]{"csv"};
            File folder = new File(path);
            return (List<File>) FileUtils.listFiles(folder, extensions, true);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("There's not files in directory : " + path);
        }
        throw new IllegalArgumentException();
    }
}
